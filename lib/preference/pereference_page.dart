import 'package:change_theme/ui/global/themes/app_themes.dart';
import 'package:change_theme/ui/global/themes/bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PreferencePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Preferences"),
      ),
      body: ListView.builder(
        padding: EdgeInsets.all(8),
        itemCount: AppTheme.values.length,
        itemBuilder: (context, index) {
          final itemApptheme = AppTheme.values[index];
          return Card(
            color: appThemeData[itemApptheme].primaryColor,
            child: ListTile(
              title: Text(
                itemApptheme.toString(),
                style: appThemeData[itemApptheme].textTheme.body1,
              ),
              onTap: () {
                BlocProvider.of<ThemeBloc>(context).dispatch(
                  ThemeChaged(theme: itemApptheme),
                );
              },
            ),
          );
        },
      ),
    );
  }
}
