import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:change_theme/ui/global/themes/app_themes.dart';
import './bloc.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  @override
  ThemeState get initialState =>
      ThemeState(themeData: appThemeData[AppTheme.GreenLight]);

  @override
  Stream<ThemeState> mapEventToState(
    ThemeEvent event,
  ) async* {
    if (event is ThemeChaged) {
      yield ThemeState(themeData: appThemeData[event.theme]);
    }
  }
}
