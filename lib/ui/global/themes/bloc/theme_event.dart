import 'package:change_theme/ui/global/themes/app_themes.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ThemeEvent extends Equatable {
  ThemeEvent([List props = const <dynamic>[]]) : super(props);
}

class ThemeChaged extends ThemeEvent {
  final AppTheme theme;

  ThemeChaged({@required this.theme}) : super([theme]);
}
